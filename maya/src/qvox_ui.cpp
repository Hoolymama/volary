
/////////////////////////////////////////////////////////////////////
#include <maya/MMaterial.h>
#include <maya/MDagPath.h>

#include <maya/MSelectionMask.h>
#include <maya/MQuaternion.h>
#include <maya/MTransformationMatrix.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MFnNurbsCurveData.h>
// #include <maya/.h>
#include "errorMacros.h"
#include "mayaMath.h"

#include "qvox_data.h"
#include "qvox_shape.h"
#include "qvox_ui.h"

#if defined(OSMac_MachO_)
#include <OpenGL/glu.h>
#else
#include <GL/glu.h>
#endif


#define LEAD_COLOR              18  // green
#define ACTIVE_COLOR            15  // white
#define ACTIVE_AFFECTED_COLOR   8   // purple
#define DORMANT_COLOR           4   // blue
#define HILITE_COLOR            17  // pale blue

const double rad_to_deg = (180 / 3.1415927);
const double deg_to_rad = 0.01745329238;

qvoxUI::qvoxUI() {
    m_box_quads = glGenLists(2);
    m_box_wire = m_box_quads + 1;
    glNewList(m_box_quads, GL_COMPILE);
    glBegin(GL_QUADS);

    // -y
    glNormal3f( 0.0f, -0.5f, 0.0f);
    glVertex3f(-0.5f, -0.5f, -0.5f);
    glVertex3f(0.5f, -0.5f, -0.5f);
    glVertex3f(0.5f, -0.5f, 0.5f);
    glVertex3f(-0.5f, -0.5f, 0.5f);

    // +z
    glNormal3f( 0.0f, 0.0f, 0.5f);
    glVertex3f(-0.5f, -0.5f, 0.5f);
    glVertex3f(0.5f, -0.5f, 0.5f);
    glVertex3f(0.5f, 0.5f, 0.5f);
    glVertex3f(-0.5f, 0.5f, 0.5f);

    // -z
    glNormal3f( 0.0f, 0.0f, -0.5f);
    glVertex3f(-0.5f, -0.5f, -0.5f);
    glVertex3f(-0.5f, 0.5f, -0.5f);
    glVertex3f(0.5f, 0.5f, -0.5f);
    glVertex3f(0.5f, -0.5f, -0.5f);

    // +x
    glNormal3f( 0.5f, 0.0f, 0.0f);
    glVertex3f(0.5f, -0.5f, -0.5f);
    glVertex3f(0.5f, 0.5f, -0.5f);
    glVertex3f(0.5f, 0.5f, 0.5f);
    glVertex3f(0.5f, -0.5f, 0.5f);

    // -x
    glNormal3f( -0.5f, 0.0f, 0.0f);
    glVertex3f(-0.5f, -0.5f, -0.5f);
    glVertex3f(-0.5f, -0.5f, 0.5f);
    glVertex3f(-0.5f, 0.5f, 0.5f);
    glVertex3f(-0.5f, 0.5f, -0.5f);

    // +y
    glNormal3f( 0.0f, 0.5f, 0.0f);
    glVertex3f(-0.5f, 0.5f, -0.5f);
    glVertex3f(-0.5f, 0.5f, 0.5f);
    glVertex3f(0.5f, 0.5f, 0.5f);
    glVertex3f(0.5f, 0.5f, -0.5f);

    glEnd();
    glEndList();

    glNewList(m_box_wire, GL_COMPILE);
    // -y
    glBegin(GL_LINE_STRIP);
    glVertex3f(-0.5f, -0.5f, -0.5f);
    glVertex3f(0.5f, -0.5f, -0.5f);
    glVertex3f(0.5f, -0.5f, 0.5f);
    glVertex3f(-0.5f, -0.5f, 0.5f);
    glVertex3f(-0.5f, -0.5f, -0.5f);
    glEnd();


    // +y
    glBegin(GL_LINE_STRIP);
    glVertex3f(-0.5f, 0.5f, -0.5f);
    glVertex3f(-0.5f, 0.5f, 0.5f);
    glVertex3f(0.5f, 0.5f, 0.5f);
    glVertex3f(0.5f, 0.5f, -0.5f);
    glVertex3f(-0.5f, 0.5f, -0.5f);
    glEnd();

    // joining verticals
    glBegin(GL_LINES);
    glVertex3f(-0.5f, 0.5f, -0.5f);
    glVertex3f(-0.5f, -0.5f, -0.5f);

    glVertex3f(-0.5f, 0.5f, 0.5f);
    glVertex3f(-0.5f, -0.5f, 0.5f);

    glVertex3f(0.5f, 0.5f, 0.5f);
    glVertex3f(0.5f, -0.5f, 0.5f);

    glVertex3f(0.5f, 0.5f, -0.5f);
    glVertex3f(0.5f, -0.5f, -0.5f);
    glEnd();

    glEndList();

}

qvoxUI::~qvoxUI() {}

void *qvoxUI::creator()
{
    return new qvoxUI();
}

bool qvoxUI::wireframeOnShadedStatus(const MDrawInfo &info) const
{
    M3dView::DisplayStatus displayStatus = info.displayStatus();
    return (
               (displayStatus == M3dView::kActive) ||
               (displayStatus == M3dView::kLead) ||
               (displayStatus == M3dView::kHilite)
           );
}

void qvoxUI::addShadedRequestToQueue(
    const MDrawInfo &info,
    MDrawRequestQueue &queue,
    MDrawData &data )
{


    MDrawRequest request = info.getPrototype( *this );
    request.setDrawData( data );
    request.setToken( kDrawFlatShaded );
    M3dView view = info.view();

    MDagPath path = info.multiPath();
    MMaterial material = MPxSurfaceShapeUI::material( path );
    M3dView::DisplayStatus displayStatus = info.displayStatus();


    material.evaluateMaterial( view, path ) ;
    request.setMaterial( material );
    bool materialTransparent = false;
    material.getHasTransparency( materialTransparent );
    if ( materialTransparent )  { request.setIsTransparent( true ); }

    queue.add( request );

}


void qvoxUI::addWireframeRequestToQueue(
    const MDrawInfo &info,
    MDrawRequestQueue &queue,
    MDrawData &data ,
    int drawToken)
{


    MDrawRequest request = info.getPrototype( *this );
    request.setDrawData( data );

    request.setToken( drawToken );
    request.setDisplayStyle( M3dView::kWireFrame );


    M3dView::DisplayStatus displayStatus = info.displayStatus();
    M3dView::ColorTable activeColorTable = M3dView::kActiveColors;
    M3dView::ColorTable dormantColorTable = M3dView::kDormantColors;


    switch ( displayStatus )
    {
        case M3dView::kLead :
            request.setColor( LEAD_COLOR, activeColorTable );
            break;
        case M3dView::kActive :
            request.setColor( ACTIVE_COLOR, activeColorTable );
            break;
        case M3dView::kActiveAffected :
            request.setColor( ACTIVE_AFFECTED_COLOR, activeColorTable );
            break;
        case M3dView::kDormant :
            request.setColor( DORMANT_COLOR, dormantColorTable );
            break;
        case M3dView::kHilite :
            request.setColor( HILITE_COLOR, activeColorTable );
            break;
        default:
            break;
    }

    queue.add( request );

}


void qvoxUI::addEdgesRequestToQueue(
    const MDrawInfo &info,
    MDrawRequestQueue &queue,
    MDrawData &data ,
    int drawToken)
{

    MDrawRequest request = info.getPrototype( *this );
    request.setDrawData( data );
    request.setToken( drawToken );

    queue.add( request );

}


void qvoxUI::getDrawRequests( const MDrawInfo &info,
                              bool objectAndActiveOnly,
                              MDrawRequestQueue &queue )
{

    M3dView::DisplayStyle dStyle = info.displayStyle();
    qvoxShape *shapeNode = (qvoxShape *)surfaceShape();
    qvoxData *geom = shapeNode->geometry();

    MDrawData data;
    getDrawData( geom, data );

    if (dStyle ==  M3dView::kWireFrame ) {
        addWireframeRequestToQueue( info, queue, data , kDrawWireframe );
        // if (geom-m_drawEdges == true) addEdgesRequestToQueue( info, queue, data , kDrawEdges );
    }
    else if ((dStyle ==  M3dView::kGouraudShaded) || (dStyle ==  M3dView::kFlatShaded)) {
        addShadedRequestToQueue( info, queue, data );
        if (geom->m_drawEdges == true) { addEdgesRequestToQueue( info, queue, data , kDrawEdges ); }
        if (wireframeOnShadedStatus(info)) {
            addWireframeRequestToQueue( info, queue, data, kDrawWireframeOnShaded );
        }

    }
}

void qvoxUI::draw( const MDrawRequest &request, M3dView &view ) const
{

    int token = request.token();
    switch ( token )
    {
        case kDrawWireframe :
            drawWireframeVoxels(request, view);
            break;
        case kDrawWireframeOnShaded :
            drawWireframeVoxels(request, view);
            break;
        case kDrawSmoothShaded :
            drawShadedVoxels(request, view);
            break;
        case kDrawFlatShaded :
            drawShadedVoxels(request, view);
            break;
        case kDrawEdges :
            drawEdges(request, view);
            break;
    }

}

void qvoxUI::drawWireframeVoxels( const MDrawRequest &request, M3dView &view ) const {

    MDrawData data = request.drawData();
    qvoxData *geom = (qvoxData *)data.geometry();
    if (geom->m_isValid == false) { return; }
    unsigned pl = geom->m_positions.length();
    glPushMatrix();
    view.beginGL();
    for (unsigned i = 0; i < pl; i++) {
        const MVector &pos = geom->m_positions[i];
        const double &boxScale = geom->m_scales[i];
        glPushMatrix();
        glTranslated(pos.x, pos.y, pos.z);
        glScaled(boxScale, boxScale, boxScale);
        glCallList(m_box_wire);
        glPopMatrix();
    }
    glPopMatrix();
    view.endGL();
}

// void qvoxUI::bitUnpack(unsigned number, MIntArray & mask) {
//     mask = MIntArray(12);
//     for (unsigned i=0;i<12;i++){
//         if ((number % 2) == 1) mask.set(1,i);
//         number = number / 2;
//     }
// }

void qvoxUI::drawShadedVoxels( const MDrawRequest &request, M3dView &view ) const {
    MStatus st;


    MDrawData data = request.drawData();
    qvoxData *geom = (qvoxData *)data.geometry();
    if (geom->m_isValid == false) { return; }

    unsigned pl = geom->m_positions.length();

    bool drawTexture = false;

    view.beginGL();

    bool isDiffuse = (geom->m_diffuse > 0.0f);
    bool isEmissive = (geom->m_incandescent > 0.0f);

    glPushAttrib(GL_LIGHTING_BIT);

    GLfloat defaultMaterial[]  = {0.0f, 0.0f, 0.0f};
    if (!isDiffuse) {
        glMaterialfv(GL_FRONT, GL_DIFFUSE,  defaultMaterial);
    }
    if (!isEmissive) {
        glMaterialfv(GL_FRONT, GL_EMISSION,  defaultMaterial);
    }

    for (unsigned i = 0; i < pl; i++) {
        const MVector &pos = geom->m_positions[i];
        const double &boxScale = geom->m_scales[i];
        glPushMatrix();
        if (isDiffuse) {
            GLfloat diffuseMaterial[] = {
                float(geom->m_colors[i].x * geom->m_diffuse)  ,
                float(geom->m_colors[i].y * geom->m_diffuse)  ,
                float(geom->m_colors[i].z * geom->m_diffuse)
            };
            glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuseMaterial);
        }
        if (isEmissive) {
            GLfloat emissiveMaterial[] = {
                float(geom->m_colors[i].x * geom->m_incandescent)  ,
                float(geom->m_colors[i].y * geom->m_incandescent)  ,
                float(geom->m_colors[i].z * geom->m_incandescent)
            };
            glMaterialfv(GL_FRONT, GL_EMISSION, emissiveMaterial);
        }
        glTranslated(pos.x, pos.y, pos.z);
        glScaled(boxScale, boxScale, boxScale);
        glCallList(m_box_quads);




        glPopMatrix();
    }
    glPopAttrib();


    view.endGL();

}


void qvoxUI::drawEdges( const MDrawRequest &request, M3dView &view ) const {
    MStatus st;


    MDrawData data = request.drawData();
    qvoxData *geom = (qvoxData *)data.geometry();
    if (geom->m_isValid == false) { return; }

    unsigned pl = geom->m_positions.length();

    bool drawTexture = false;

    view.beginGL();

    MIntArray edgeFlags(12);


    glPushAttrib(GL_LINE_BIT);

    view.setDrawColor(geom->m_edgeColor);
    for (unsigned i = 0; i < pl; i++) {
        const MVector &pos = geom->m_positions[i];
        const double &boxScale = geom->m_scales[i];

        unsigned num = geom->m_edges[i];
        for (unsigned j = 0; j < 12; j++) {
            if ((num % 2) == 1) {
                edgeFlags.set(1, j);
            }
            else {
                edgeFlags.set(0, j);
            }
            num = num / 2;
        }

        glPushMatrix();
        glTranslated(pos.x, pos.y, pos.z);
        glScaled(boxScale, boxScale, boxScale);

        // draw the edges


        glLineWidth(GLfloat(geom->m_edgeThickness));
        // MColor c = MColor(
        //     (geom->m_colors[i].x * 0.5),
        //     (geom->m_colors[i].y * 0.5),
        //     (geom->m_colors[i].z * 0.5)
        // );
        // view.setDrawColor(MColor(0.0f,0.0f,0.0f));
        glBegin(GL_LINES);
        if (edgeFlags[0]) { //e_lx_ly
            glVertex3f(-0.5f, -0.5f, -0.5f);
            glVertex3f(-0.5f, -0.5f, 0.5f);
        }
        if (edgeFlags[1]) { //e_lx_hy
            glVertex3f(-0.5f, 0.5f, -0.5f);
            glVertex3f(-0.5f, 0.5f, 0.5f);
        }
        if (edgeFlags[2]) { //e_hx_ly
            glVertex3f(0.5f, -0.5f, -0.5f);
            glVertex3f(0.5f, -0.5f, 0.5f);
        }
        if (edgeFlags[3]) { //e_hx_hy
            glVertex3f(0.5f, 0.5f, -0.5f);
            glVertex3f(0.5f, 0.5f, 0.5f);
        }
        if (edgeFlags[4]) { //e_ly_lz
            glVertex3f(-0.5f, -0.5f, -0.5f);
            glVertex3f(0.5f, -0.5f, -0.5f);
        }
        if (edgeFlags[5]) { //e_ly_hz
            glVertex3f(-0.5f, -0.5f, 0.5f);
            glVertex3f(0.5f, -0.5f, 0.5f);
        }
        if (edgeFlags[6]) { //e_hy_lz
            glVertex3f(-0.5f, 0.5f, -0.5f);
            glVertex3f(0.5f, 0.5f, -0.5f);
        }
        if (edgeFlags[7]) { //e_hy_hz
            glVertex3f(-0.5f, 0.5f, 0.5f);
            glVertex3f(0.5f, 0.5f, 0.5f);
        }
        if (edgeFlags[8]) { //e_lz_lx
            glVertex3f(-0.5f, -0.5f, -0.5f);
            glVertex3f(-0.5f, 0.5f, -0.5f);
        }
        if (edgeFlags[9]) { //e_lz_hx
            glVertex3f(0.5f, -0.5f, -0.5f);
            glVertex3f(0.5f, 0.5f, -0.5f);
        }
        if (edgeFlags[10]) { //e_hz_lx
            glVertex3f(-0.5f, -0.5f, 0.5f);
            glVertex3f(-0.5f, 0.5f, 0.5f);
        }
        if (edgeFlags[11]) { //e_hz_hx
            glVertex3f(0.5f, -0.5f, 0.5f);
            glVertex3f(0.5f, 0.5f, 0.5f);
        }
        glEnd();
        glPopMatrix();
    }


    glPopAttrib();

    view.endGL();

}



bool qvoxUI::select( MSelectInfo &selectInfo,
                     MSelectionList &selectionList,
                     MPointArray &worldSpaceSelectPts ) const
//
// Select function. Gets called when the bbox for the object is selected.
// This function just selects the object without doing any intersection tests.
//
{

    MSelectionMask priorityMask( MSelectionMask::kSelectObjectsMask );
    MSelectionList item;
    item.add( selectInfo.selectPath() );
    MPoint xformedPt;
    selectInfo.addSelection(
        item, xformedPt,
        selectionList,
        worldSpaceSelectPts,
        priorityMask, false
    );
    return true;
}


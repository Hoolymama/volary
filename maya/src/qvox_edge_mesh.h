/*
 *  qvoxEdgeMesh.h
 *  jtools
 *
 *  Created by jmann on 10/29/08.
 *  Copyright 2008 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _qvoxEdgeMesh
#define _qvoxEdgeMesh


#if defined(linux)
#ifndef LINUX
#define LINUX
#endif
#endif


#include <maya/MObject.h>
#include <maya/MGlobal.h>
#include <maya/MDataBlock.h>
#include <maya/MPointArray.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatVectorArray.h>

#include <maya/MIntArray.h>
#include <maya/MPxLocatorNode.h>
#include <maya/MBoundingBox.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFloatPointArray.h>
 
 class qvoxEdgeMesh : public MPxNode
 {
 public:
 	qvoxEdgeMesh();
 	virtual				~qvoxEdgeMesh(); 

 	virtual MStatus		compute( const MPlug& plug, MDataBlock& data );
 	static  void*		creator();
 	static  MStatus		initialize();
 	
 	static  MObject     aPoints;
 	static  MObject 	aCubeScales;
 	static  MObject 	aEdges;
 	static  MObject 	aEdgeWidth;
 	static  MObject 	aEdgeOffset;
 	static  MObject     aOutMesh;
 	static	MTypeId		id;

	static const unsigned edgeVerts[];

	static const MMatrix rmat_lx_ly;
	static const MMatrix rmat_lx_hy;
	static const MMatrix rmat_hx_ly;
	static const MMatrix rmat_hx_hy;
	static const MMatrix rmat_ly_lz;
	static const MMatrix rmat_ly_hz;
	static const MMatrix rmat_hy_lz;
	static const MMatrix rmat_hy_hz;
	static const MMatrix rmat_lz_lx;
	static const MMatrix rmat_lz_hx;
	static const MMatrix rmat_hz_lx;
	static const MMatrix rmat_hz_hx;
 private:

 	MStatus makeEdges(
 		const MPoint pos,
 		double scale,
 		unsigned edgeMask,
 		float edgeWidth,
 		float edgeOffset,
 		MPointArray & outVertices, 
 		MIntArray & outFaceCounts,
 		MIntArray &  outConnectivity
 		) ;

 	MStatus makeEdge(
	const MPoint &pos,
	const MMatrix & mat,
	const MPointArray &verts,	
	MPointArray & outVertices, 
	MIntArray & outFaceCounts,
	MIntArray &  outConnectivity);

 };

#endif


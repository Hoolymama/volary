#ifndef _qvoxUI
#define _qvoxUI

#if defined(linux)
#ifndef LINUX
#define LINUX
#endif
#endif

#if defined(OSMac_MachO_)
#include <OpenGL/glu.h>
#else
#include <GL/glu.h>
#endif

#include <maya/MIOStream.h>
#include <maya/MPxSurfaceShape.h>
#include <maya/MPxSurfaceShapeUI.h>
#include <maya/MDrawData.h>
#include <maya/MDrawRequest.h>
#include <maya/MSelectionList.h>

#include "mayaMath.h"

/////////////////////////////////////////////////////////////////////
class qvoxUI : public MPxSurfaceShapeUI
{
public:
	qvoxUI();
	virtual ~qvoxUI(); 
	static  void *  creator();

	virtual void	getDrawRequests( const MDrawInfo & info,
		bool objectAndActiveOnly,
		MDrawRequestQueue & requests );

	virtual void	draw( const MDrawRequest & request,
		M3dView & view ) const;


	virtual bool	select( MSelectInfo &selectInfo,
		MSelectionList &selectionList,
		MPointArray &worldSpaceSelectPts ) const;

	bool wireframeOnShadedStatus(const MDrawInfo& info) const;

	void addShadedRequestToQueue( 
		const MDrawInfo& info,
		MDrawRequestQueue& queue,
		MDrawData& data ) ;

	void addWireframeRequestToQueue( 
		const MDrawInfo& info,
		MDrawRequestQueue& queue,
		MDrawData& data ,
		int drawToken) ;
	
	void addEdgesRequestToQueue( 
		const MDrawInfo& info,
		MDrawRequestQueue& queue,
		MDrawData& data ,
		int drawToken) ;

	void drawWireframeVoxels( const MDrawRequest & request, M3dView & view ) const;
	
	void drawShadedVoxels( const MDrawRequest & request, M3dView & view ) const ;
	
	void drawEdges( const MDrawRequest & request, M3dView & view ) const ;
	// void bitUnpack(unsigned number, MIntArray & mask) ;


private:

	enum {
		kDrawWireframe,
		kDrawWireframeOnShaded,
		kDrawSmoothShaded,
		kDrawFlatShaded,
		kDrawEdges,
		kLastToken
	};

	GLuint m_box_quads;
	GLuint m_box_wire;
};
#endif

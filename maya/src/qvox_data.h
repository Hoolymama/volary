#ifndef _qvoxData
#define _qvoxData


#include <maya/MVectorArray.h>
#include <maya/MMatrix.h>
#include <maya/MIntArray.h>
#include <maya/MDoubleArray.h>
#include <maya/MMatrix.h>
#include <maya/MColor.h>

class qvoxData 
{
public:
	qvoxData();
	~qvoxData();

	MVectorArray m_positions;
	MMatrix m_matrix;
	MVectorArray m_colors;
	MDoubleArray m_scales;
	MIntArray m_edges;
	MColor m_edgeColor;
	float m_edgeThickness;
	float m_diffuse;
	float m_incandescent;
	bool m_drawEdges;
	bool m_isValid;

};


#endif
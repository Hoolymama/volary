/*
 *  qvoxMesh.h
 *  jtools
 *
 *  Created by jmann on 10/29/08.
 *  Copyright 2008 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _qvoxMesh
#define _qvoxMesh


#if defined(linux)
#ifndef LINUX
#define LINUX
#endif
#endif


#include <maya/MObject.h>
#include <maya/MGlobal.h>
#include <maya/MDataBlock.h>
#include <maya/MPointArray.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatVectorArray.h>

#include <maya/MIntArray.h>
#include <maya/MPxLocatorNode.h>
#include <maya/MBoundingBox.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFloatPointArray.h>
 
 class qvoxMesh : public MPxNode
 {
 public:
 	qvoxMesh();
 	virtual				~qvoxMesh(); 

 	virtual MStatus		compute( const MPlug& plug, MDataBlock& data );
 	static  void*		creator();
 	static  MStatus		initialize();
 	
 	static  MObject     aPoints;
 	static  MObject 	aCubeScales;
 	static  MObject 	aCubeColors;
 	// static  MObject 	aEdges;
 	// static  MObject 	aEdgeWidth;
 	// static  MObject 	aEdgeOffset;

 	static  MObject     aOutMesh;
 	static	MTypeId		id;

	static const unsigned cubeVerts[];

 private:

 	MStatus makeCube(
 		const MPoint pos,
 		double scale,
 		MPointArray & outVertices, 
 		MIntArray & outFaceCounts,
 		MIntArray &  outConnectivity
 		) ;

 };

#endif


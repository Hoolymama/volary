

#include <maya/MStatus.h>
#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>

#include "errorMacros.h"

#include "qvox_ui.h"
#include "qvox_shape.h"
#include "qvox_mesh.h"
#include "qvox_edge_mesh.h"


MStatus initializePlugin( MObject obj )
{

	MStatus st;

	MString method("initializePlugin");

	MFnPlugin plugin( obj, PLUGIN_VENDOR, PLUGIN_VERSION , MAYA_VERSION);



	st = plugin.registerNode( "voxelEdgeMesh", qvoxEdgeMesh::id, qvoxEdgeMesh::creator,
	                          qvoxEdgeMesh::initialize); ert;
	st = plugin.registerNode( "voxelMesh", qvoxMesh::id, qvoxMesh::creator,
	                          qvoxMesh::initialize); ert;


	st = plugin.registerShape(
	       "voxelPreviewShape",
	       qvoxShape::id,
	       qvoxShape::creator,
	       qvoxShape::initialize,
	       qvoxUI::creator ); er;

	MGlobal::executePythonCommand("import volary;volary.load()");

	return st;
}

MStatus uninitializePlugin( MObject obj)
{
	MStatus st;

	MString method("uninitializePlugin");

	MFnPlugin plugin( obj );
	st = plugin.deregisterNode( qvoxShape::id ); er;

	st = plugin.deregisterNode( qvoxMesh::id ); ert;
	st = plugin.deregisterNode( qvoxEdgeMesh::id ); ert;




	return st;
}




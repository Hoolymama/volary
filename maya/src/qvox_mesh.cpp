#include <maya/MIOStream.h>
#include <math.h>

#include <maya/MPxNode.h>

#include <maya/MFnTypedAttribute.h>

#include <maya/MFnMesh.h>
#include <maya/MFnMeshData.h>
#include <maya/MColorArray.h>
#include <maya/MPointArray.h>
#include <maya/MFnVectorArrayData.h> 
#include <maya/MFnDoubleArrayData.h>
#include <maya/MTypeId.h> 
#include <maya/MPlug.h> 
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h> 
#include <maya/MMatrix.h> 
#include <maya/MFnMatrixAttribute.h> 
#include <maya/MFnMatrixData.h> 


#include "errorMacros.h"
#include "jMayaIds.h"

#include "qvox_mesh.h" 

#define kTOLERANCE 0.0001

MTypeId qvoxMesh::id( k_qvoxMesh  );

 	
MObject qvoxMesh::aPoints;
MObject qvoxMesh::aCubeScales;
MObject qvoxMesh::aCubeColors;
// MObject qvoxMesh::aEdges;
// MObject qvoxMesh::aEdgeWidth;
// MObject qvoxMesh::aEdgeOffset;


MObject qvoxMesh::aOutMesh;


// these 24 numbers are vertex ID offsets for face connectivity for the 6 quads
// that make a cube
const unsigned qvoxMesh::cubeVerts[] = {0,3,2,1,3,7,6,2,1,2,6,5,4,7,3,0,5,6,7,4,0,1,5,4}; 


qvoxMesh::qvoxMesh()
{}
qvoxMesh::~qvoxMesh() {}

void* qvoxMesh::creator()
{
	return new qvoxMesh();
}

MStatus qvoxMesh::makeCube(
	const MPoint pos,
	double scale,
	// const MMatrix &mat,
	MPointArray & outVertices, 
	MIntArray & outFaceCounts,
	MIntArray &  outConnectivity
) 
{
	MStatus st;
	unsigned startIndex = outVertices.length();
	double h = scale * 0.5;

	outVertices.append( pos + MVector(-h,-h,-h) );
	outVertices.append( pos + MVector(h,-h,-h)  );
	outVertices.append( pos + MVector(h,h,-h)   );
	outVertices.append( pos + MVector(-h,h,-h)  );
	outVertices.append( pos + MVector(-h,-h,h)  );
	outVertices.append( pos + MVector(h,-h,h)   );
	outVertices.append( pos + MVector(h,h,h)    );
	outVertices.append( pos + MVector(-h,h,h)   );
	for (int i = 0; i < 6; ++i)
	{
		outFaceCounts.append(4);
	}
	for (int i = 0; i < 24; ++i)
	{
		outConnectivity.append(cubeVerts[i]  + startIndex);
	}
	return MS::kSuccess;
}


MStatus qvoxMesh::compute( const MPlug& plug, MDataBlock& data )	{
	
	MStatus st;

	if(! ( plug == aOutMesh ) ) return MS::kUnknownParameter;


	MFnVectorArrayData fnV;
	MFnDoubleArrayData fnD;
	MFnDoubleArrayData fnI;
	MDataHandle h;
	MObject d;

	unsigned pl = 0;

	h = data.inputValue(aPoints, &st);ert;
	d = h.data();
	st = fnV.setObject(d);ert;
	pl = fnV.length();
	const MVectorArray & pos = fnV.array();	


	MDoubleArray scales;
	bool scalesValid = false;
	h = data.inputValue(aCubeScales, &st);ert;
	d = h.data();
	st = fnD.setObject(d);ert;
	if (fnD.length() == pl) {
		scales= fnD.array();
		scalesValid = true;
	}


	MVectorArray colors ;
	bool colorsValid = false;
	h = data.inputValue(aCubeColors, &st);
	if (! st.error()) {
		d = h.data();
		st = fnV.setObject(d);
		if (! st.error()) {
			if (fnV.length() == pl) {
				colors = fnV.array();	
				colorsValid = true;
			}	
		}
	}


	// MDoubleArray edges;
	// bool edgesValid = false;
	// h = data.inputValue(aEdges, &st);ert;
	// d = h.data();
	// st = fnI.setObject(d);ert;
	// if (fnI.length() == pl) {
	// 	edges = fnI.array();
	// 	edgesValid = true;
	// }



	MFnMesh fnM;
	MFnMeshData fnC;
	MObject outGeom = fnC.create(&st);er;
	// clean the mesh if no points


	if ( !(pl  && scalesValid) ) {
		
		fnM.create( 0, 0, MFloatPointArray(), MIntArray(), MIntArray(), outGeom, &st );  er;
		MDataHandle hMesh = data.outputValue(aOutMesh, &st);
		hMesh.set(outGeom);
		data.setClean(aOutMesh);
		return MS::kUnknownParameter;	
	}


	MPointArray outVertices;
	MIntArray outFaceCounts;
	MIntArray outConnectivity;	



	for (int i = 0; i < pl; ++i)
	{
		makeCube(
			pos[i],scales[i],
			outVertices,outFaceCounts,outConnectivity
		);
	}


	unsigned nVerts = outVertices.length();

	fnM.create( nVerts, outFaceCounts.length(), 
        outVertices, outFaceCounts, 
        outConnectivity, outGeom, &st 
    );  er;


	if (colorsValid) {
		MIntArray vertList(nVerts);
		MColorArray colorList(nVerts);
		unsigned i = 0;
		for (unsigned j = 0; j < pl; ++j)
			{

			for (int k = 0; k < 8; ++k)
			{
				colorList.set(MColor(colors[j].x,colors[j].y,colors[j].z ),i);
				vertList.set(i,i);
				i++;
			}
		}
		fnM.setVertexColors(colorList,vertList);
	}

	// hard edges
	for (int i=0; i<fnM.numEdges(); i++) fnM.setEdgeSmoothing(i, 0);
	fnM.cleanupEdgeSmoothing();

	MDataHandle hMesh = data.outputValue(aOutMesh, &st);
	hMesh.set(outGeom);
	data.setClean(aOutMesh);

	return MS::kSuccess;
}



MStatus qvoxMesh::initialize()
{
	
	MStatus st;

	MFnTypedAttribute   tAttr;
	// MFnNumericAttribute   nAttr;
	// MFnMatrixAttribute		mAttr;

	////////////////////////////////////////////////////////////////////
	// aVoxelMatrix = mAttr.create("voxelMatrix","vm");
	// mAttr.setReadable( false );
	// mAttr.setStorable( true );
	// addAttribute( aVoxelMatrix );

	aPoints = tAttr.create("positions", "pos", MFnData::kVectorArray);
    tAttr.setStorable(true);
	//tAttr.setDisconnectBehavior(MFnAttribute::kReset);
	// tAttr.setCached(false);
	st = addAttribute(aPoints);er;

	aCubeColors = tAttr.create("cubeColors", "ccol", MFnData::kVectorArray);
    tAttr.setStorable(true);
	//tAttr.setDisconnectBehavior(MFnAttribute::kReset);
	// tAttr.setCached(false);
	st = addAttribute(aCubeColors);er;

	aCubeScales = tAttr.create("cubeScales", "cscl", MFnData::kDoubleArray);
    tAttr.setStorable(true);
	//tAttr.setDisconnectBehavior(MFnAttribute::kReset);
	st = addAttribute(aCubeScales);er;

	// aEdges = tAttr.create("edges", "edg", MFnData::kIntArray);
	// tAttr.setStorable(false);
	// tAttr.setDisconnectBehavior(MFnAttribute::kReset);
	// st = addAttribute(aEdges);er;

	// aEdgeWidth = nAttr.create("edgeWidth","edw", MFnNumericData::kFloat);
	// nAttr.setStorable(true);
	// nAttr.setWritable(true);
	// nAttr.setKeyable(true);	
	// nAttr.setReadable(true);
	// nAttr.setHidden(false);
	// nAttr.setDefault(0.1f);
	// st = addAttribute(aEdgeWidth);er;

	// aEdgeOffset = nAttr.create("edgeOffset","edo", MFnNumericData::kFloat);
	// nAttr.setStorable(true);
	// nAttr.setWritable(true);
	// nAttr.setKeyable(true);	
	// nAttr.setReadable(true);
	// nAttr.setHidden(false);
	// nAttr.setDefault(0.1f);
	// st = addAttribute(aEdgeOffset);er;

	aOutMesh = tAttr.create( "outMesh", "out", MFnData::kMesh, &st );er
	tAttr.setReadable(true);
	tAttr.setStorable(false);	
	st = addAttribute(aOutMesh); er;
	///////////////////////////////////////////////////////////////////////
   
	// st = attributeAffects(aVoxelMatrix, aOutMesh );
	st = attributeAffects(aPoints, aOutMesh );
	st = attributeAffects(aCubeColors, aOutMesh );
	st = attributeAffects(aCubeScales, aOutMesh );
	// st = attributeAffects(aEdges, aOutMesh );
	// st = attributeAffects(aEdgeWidth, aOutMesh );
	// st = attributeAffects(aEdgeOffset, aOutMesh );
	return MS::kSuccess;
}




#include <maya/MIOStream.h>
#include <math.h>

#include <maya/MPxNode.h>

#include <maya/MFnTypedAttribute.h>

#include <maya/MFnMesh.h>
#include <maya/MFnMeshData.h>
#include <maya/MColorArray.h>
#include <maya/MPointArray.h>
#include <maya/MFnVectorArrayData.h> 
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnIntArrayData.h>

#include <maya/MTypeId.h> 
#include <maya/MPlug.h> 
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h> 
#include <maya/MMatrix.h> 
#include <maya/MFnMatrixAttribute.h> 

#include <maya/MFnNumericAttribute.h> 

#include <maya/MFnMatrixData.h> 
#include <maya/MEulerRotation.h> 


#include "errorMacros.h"
#include "jMayaIds.h"

#include "qvox_edge_mesh.h" 

#define kTOLERANCE 0.0001

const double deg90 = 1.570796;
const double deg180 = 3.141593;

MTypeId qvoxEdgeMesh::id( k_qvoxEdgeMesh  );

 	
MObject qvoxEdgeMesh::aPoints;
MObject qvoxEdgeMesh::aCubeScales;
MObject qvoxEdgeMesh::aEdges;
MObject qvoxEdgeMesh::aEdgeWidth;
MObject qvoxEdgeMesh::aEdgeOffset;


MObject qvoxEdgeMesh::aOutMesh;


// these 8 numbers are vertex ID offsets for face connectivity for the 2 quads
// that make an edge
const unsigned qvoxEdgeMesh::edgeVerts[] = {0,3,4,1,1,4,5,2}; 

const MMatrix qvoxEdgeMesh::rmat_lx_ly = MEulerRotation(MVector::zero).asMatrix();
const MMatrix qvoxEdgeMesh::rmat_lx_hy = MEulerRotation(MVector(0.0,0.0,-deg90)).asMatrix();
const MMatrix qvoxEdgeMesh::rmat_hx_ly = MEulerRotation(MVector(0.0,0.0,deg90)).asMatrix();
const MMatrix qvoxEdgeMesh::rmat_hx_hy = MEulerRotation(MVector(0.0,0.0,deg180)).asMatrix();
const MMatrix qvoxEdgeMesh::rmat_ly_lz = MEulerRotation(MVector(0.0,-deg90,0.0)).asMatrix();
const MMatrix qvoxEdgeMesh::rmat_ly_hz = MEulerRotation(MVector(0.0,deg90,0.0)).asMatrix();
const MMatrix qvoxEdgeMesh::rmat_hy_lz = MEulerRotation(MVector(deg90,0.0,-deg90)).asMatrix();
const MMatrix qvoxEdgeMesh::rmat_hy_hz = MEulerRotation(MVector(0.0,deg90,deg180)).asMatrix();
const MMatrix qvoxEdgeMesh::rmat_lz_lx = MEulerRotation(MVector(deg90,0.0,0.0)).asMatrix();
const MMatrix qvoxEdgeMesh::rmat_lz_hx = MEulerRotation(MVector(deg180,-deg90,-deg90)).asMatrix();
const MMatrix qvoxEdgeMesh::rmat_hz_lx = MEulerRotation(MVector(deg180,deg90,deg90)).asMatrix();
const MMatrix qvoxEdgeMesh::rmat_hz_hx = MEulerRotation(MVector(-deg90,0.0,deg180)).asMatrix();


qvoxEdgeMesh::qvoxEdgeMesh()
{}
qvoxEdgeMesh::~qvoxEdgeMesh() {}

void* qvoxEdgeMesh::creator()
{
	return new qvoxEdgeMesh();
}

// edge consist of 2 faces
MStatus qvoxEdgeMesh::makeEdge(
	const MPoint &pos,
	const MMatrix & mat,
	const MPointArray &verts,	
	MPointArray & outVertices, 
	MIntArray & outFaceCounts,
	MIntArray &  outConnectivity)
{
	unsigned startIndex = outVertices.length();
	for (int i = 0; i < 6; ++i) {
		outVertices.append( pos + (verts[i] * mat));
	}
	outFaceCounts.append(4);
	outFaceCounts.append(4);
	for (int i = 0; i < 8; ++i) {
		outConnectivity.append(edgeVerts[i]  + startIndex);
	}
	return MS::kSuccess;
}

MStatus qvoxEdgeMesh::makeEdges(
	const MPoint pos,
	double scale,
	unsigned edgeMask,
	float edgeWidth,
	float edgeOffset,
	MPointArray & outVertices, 
	MIntArray & outFaceCounts,
	MIntArray &  outConnectivity
	)
{
	MStatus st;
	unsigned startIndex = outVertices.length();
	double h = (scale * 0.5);
	double hout = h + edgeOffset;
	double hin = hout - edgeWidth;

	// make an edge at lx_ly and rotate for all the others.
	MPointArray pts(6);
	pts.set(MPoint(-hin,	-hout,  -hout),0);
	pts.set(MPoint(-hout,	-hout, 	-hout),1);
	pts.set(MPoint(-hout, 	-hin, 	-hout),2);
	pts.set(MPoint(-hin, 	-hout,  hout),3);
	pts.set(MPoint(-hout,	-hout,  hout),4);
	pts.set(MPoint(-hout, 	-hin,  	hout),5);

	// create the edge bit array
  	MIntArray edgeFlags(12);
    for (unsigned j=0;j<12;j++){
        if ((edgeMask % 2) == 1) {
            edgeFlags.set(1,j);
        }
        edgeMask = edgeMask / 2;
    }

 	if (edgeFlags[0]) { // lx_ly
		makeEdge(pos,rmat_lx_ly,pts,outVertices,outFaceCounts,outConnectivity);
    }
    if (edgeFlags[1]) { // lx_hy
		makeEdge(pos,rmat_lx_hy,pts,outVertices,outFaceCounts,outConnectivity);
    }
    if (edgeFlags[2]) { // hx_ly
		makeEdge(pos,rmat_hx_ly,pts,outVertices,outFaceCounts,outConnectivity);
    }
    if (edgeFlags[3]) { // hx_hy
		makeEdge(pos,rmat_hx_hy,pts,outVertices,outFaceCounts,outConnectivity);
    }
    if (edgeFlags[4]) { // ly_lz
		makeEdge(pos,rmat_ly_lz,pts,outVertices,outFaceCounts,outConnectivity);
    }
    if (edgeFlags[5]) { // ly_hz
		makeEdge(pos,rmat_ly_hz,pts,outVertices,outFaceCounts,outConnectivity);
    }
    if (edgeFlags[6]) { // hy_lz
		makeEdge(pos,rmat_hy_lz,pts,outVertices,outFaceCounts,outConnectivity);
    }
    if (edgeFlags[7]) { // hy_hz
		makeEdge(pos,rmat_hy_hz,pts,outVertices,outFaceCounts,outConnectivity);
    }
    if (edgeFlags[8]) { // lz_lx
		makeEdge(pos,rmat_lz_lx,pts,outVertices,outFaceCounts,outConnectivity);
    }
    if (edgeFlags[9]) { // lz_hx
		makeEdge(pos,rmat_lz_hx,pts,outVertices,outFaceCounts,outConnectivity);
    }
    if (edgeFlags[10]) { // hz_lx
		makeEdge(pos,rmat_hz_lx,pts,outVertices,outFaceCounts,outConnectivity);
    }
    if (edgeFlags[11]) { // hz_hx
		makeEdge(pos,rmat_hz_hx,pts,outVertices,outFaceCounts,outConnectivity);
    }

	return MS::kSuccess;
}


MStatus qvoxEdgeMesh::compute( const MPlug& plug, MDataBlock& data )	{
	
	MStatus st;

	if(! ( plug == aOutMesh ) ) return MS::kUnknownParameter;


	MFnVectorArrayData fnV;
	MFnDoubleArrayData fnD;
	MFnIntArrayData fnI;
	MDataHandle h;
	MObject d;

	unsigned pl = 0;

	h = data.inputValue(aPoints, &st);ert;
	d = h.data();
	st = fnV.setObject(d);ert;
	pl = fnV.length();
	const MVectorArray & pos = fnV.array();	


	MDoubleArray scales;
	bool scalesValid = false;
	h = data.inputValue(aCubeScales, &st);ert;
	d = h.data();
	st = fnD.setObject(d);ert;
	if (fnD.length() == pl) {
		scales= fnD.array();
		scalesValid = true;
	}


	// MVectorArray colors ;
	// bool colorsValid = false;
	// h = data.inputValue(aCubeColors, &st);
	// if (! st.error()) {
	// 	d = h.data();
	// 	st = fnV.setObject(d);
	// 	if (! st.error()) {
	// 		if (fnV.length() == pl) {
	// 			colors = fnV.array();	
	// 			colorsValid = true;
	// 		}	
	// 	}
	// }


	MIntArray edges;
	bool edgesValid = false;
	h = data.inputValue(aEdges, &st);ert;
	d = h.data();
	st = fnI.setObject(d);ert;
	if (fnI.length() == pl) {
		edges = fnI.array();
		edgesValid = true;
	}



	MFnMesh fnM;
	MFnMeshData fnC;
	MObject outGeom = fnC.create(&st);er;
	// clean the mesh if no points

	if ( !(pl && scalesValid && edgesValid)) {
		
		fnM.create( 0, 0, MFloatPointArray(), MIntArray(), MIntArray(), outGeom, &st );  er;
		MDataHandle hMesh = data.outputValue(aOutMesh, &st);
		hMesh.set(outGeom);
		data.setClean(aOutMesh);
		return MS::kUnknownParameter;	
	}

	float edgeWidth = data.inputValue(aEdgeWidth).asFloat();
	float edgeOffset = data.inputValue(aEdgeOffset).asFloat();

	MPointArray outVertices;
	MIntArray outFaceCounts;
	MIntArray outConnectivity;	





	for (int i = 0; i < pl; ++i)
	{
		makeEdges(
			pos[i],scales[i], edges[i],edgeWidth,edgeOffset,
			outVertices,outFaceCounts,outConnectivity
		);
	}

	unsigned nVerts = outVertices.length();

	fnM.create( nVerts, outFaceCounts.length(), 
        outVertices, outFaceCounts, 
        outConnectivity, outGeom, &st 
    );  er;

	// if (isColorPP) {
	// 	MIntArray vertList(nVerts);
	// 	MColorArray colorList(nVerts);
	// 	unsigned i = 0;
	// 	for (unsigned j = 0; j < pl; ++j)
	// 		{

	// 		for (int k = 0; k < 8; ++k)
	// 		{
	// 			colorList.set(MColor(colors[j].x,colors[j].y,colors[j].z ),i);
	// 			vertList.set(i,i);
	// 			i++;
	// 		}
	// 	}
	// 	fnM.setVertexColors(colorList,vertList);
	// }

	// hard edges
	for (int i=0; i<fnM.numEdges(); i++) fnM.setEdgeSmoothing(i, 0);
	fnM.cleanupEdgeSmoothing();

	MDataHandle hMesh = data.outputValue(aOutMesh, &st);
	hMesh.set(outGeom);
	data.setClean(aOutMesh);

	return MS::kSuccess;
}



MStatus qvoxEdgeMesh::initialize()
{
	
	MStatus st;

	MFnTypedAttribute   tAttr;
	MFnNumericAttribute   nAttr;
	// MFnMatrixAttribute		mAttr;

	////////////////////////////////////////////////////////////////////
	// aVoxelMatrix = mAttr.create("voxelMatrix","vm");
	// mAttr.setReadable( false );
	// mAttr.setStorable( true );
	// addAttribute( aVoxelMatrix );

	aPoints = tAttr.create("positions", "pos", MFnData::kVectorArray);
    tAttr.setStorable(false);
	tAttr.setDisconnectBehavior(MFnAttribute::kReset);
	// tAttr.setCached(false);
	st = addAttribute(aPoints);er;

	// aCubeColors = tAttr.create("cubeColors", "ccol", MFnData::kVectorArray);
 //    tAttr.setStorable(false);
	// tAttr.setDisconnectBehavior(MFnAttribute::kReset);
	// // tAttr.setCached(false);
	// st = addAttribute(aCubeColors);er;

	aCubeScales = tAttr.create("cubeScales", "cscl", MFnData::kDoubleArray);
	tAttr.setStorable(false);
	tAttr.setDisconnectBehavior(MFnAttribute::kReset);
	st = addAttribute(aCubeScales);er;

	aEdges = tAttr.create("edges", "edg", MFnData::kIntArray);
	tAttr.setStorable(false);
	tAttr.setDisconnectBehavior(MFnAttribute::kReset);
	st = addAttribute(aEdges);er;

	aEdgeWidth = nAttr.create("edgeWidth","edw", MFnNumericData::kFloat);
	nAttr.setStorable(true);
	nAttr.setWritable(true);
	nAttr.setKeyable(true);	
	nAttr.setReadable(true);
	nAttr.setHidden(false);
	nAttr.setDefault(0.1f);
	st = addAttribute(aEdgeWidth);er;

	aEdgeOffset = nAttr.create("edgeOffset","edo", MFnNumericData::kFloat);
	nAttr.setStorable(true);
	nAttr.setWritable(true);
	nAttr.setKeyable(true);	
	nAttr.setReadable(true);
	nAttr.setHidden(false);
	nAttr.setDefault(0.01f);
	st = addAttribute(aEdgeOffset);er;

	aOutMesh = tAttr.create( "outMesh", "out", MFnData::kMesh, &st );er
	tAttr.setReadable(true);
	tAttr.setStorable(false);	
	st = addAttribute(aOutMesh); er;
	///////////////////////////////////////////////////////////////////////
   
	// st = attributeAffects(aVoxelMatrix, aOutMesh );
	st = attributeAffects(aPoints, aOutMesh );
	// st = attributeAffects(aCubeColors, aOutMesh );
	st = attributeAffects(aCubeScales, aOutMesh );
	st = attributeAffects(aEdges, aOutMesh );
	st = attributeAffects(aEdgeWidth, aOutMesh );
	st = attributeAffects(aEdgeOffset, aOutMesh );
	return MS::kSuccess;
}




#ifndef _qvoxShape
#define _qvoxShape

/////////////////////////////////////////////////////////////////////
#include <maya/MIOStream.h>
#include <maya/MPxSurfaceShape.h>
#include <maya/MBoundingBox.h>
#include <maya/MStatus.h>
#include <maya/MTypeId.h>
#include <maya/MVectorArray.h>
#include <maya/MDoubleArray.h>
#include <maya/MFnMesh.h>

#include "qvox_data.h"
#include "errorMacros.h"

/////////////////////////////////////////////////////////////////////
class qvoxShape : public MPxSurfaceShape
{
public:

	static	MTypeId		id;
	static	MObject aInMesh;
	static	MObject aVoxelMatrix;
	static	MObject aScale;
	static	MObject aColor;
	static	MObject aDiffuseWeight;
	static	MObject aIncandescentWeight;

	static	MObject aDrawEdges;
	static	MObject aEdgeColor;
	static	MObject aEdgeThickness;
	static	MObject aRaySeed;

	static	MObject aNumberOfRays;



	static	MObject aOutEdges;      
	static	MObject aOutPositions;      
	static	MObject aOutNormals;      
	static	MObject aOutColors;
	static	MObject aOutScales;
	static  MObject aOutBoundsMin;
	static  MObject aOutBoundsMax;

	qvoxShape();
	virtual ~qvoxShape(); 

	virtual void			postConstructor();

	virtual MStatus			compute( const MPlug&, MDataBlock& );

	virtual bool            isBounded() const;
	virtual MBoundingBox    boundingBox() const; 

	static  void *		creator();
	static  MStatus		initialize();
	MStatus getTextureName(const MObject &attribute, MString &name) const;

	unsigned bitPack(const MIntArray &mask);
	MStatus bitUnpack(unsigned number, MIntArray & mask) ;

unsigned getEdgeMask(unsigned i, const MIntArray &containment, 
	unsigned nx, unsigned ny, unsigned nz) ;
// bool isContained(
// 	MFnMesh & meshFn,
// 	const MFloatPoint &rayOrigin,
// 	unsigned nRays,
// 	MMeshIsectAccelParams & app
// );

	MStatus sampleTexture(
		const MString & sourceName,
		MFloatPointArray & pVals,
		MFloatVectorArray & fNormals,
		MFloatVectorArray & colors,
		MFloatVectorArray & transparencies
	) const;

	  	unsigned cellIndex(
 		unsigned nx,
 		unsigned ny,
 		unsigned nz,
 		unsigned x,
 		unsigned y,
 		unsigned z) ;

 	MStatus cellIndices(
 		unsigned nx,
 		unsigned ny,
 		unsigned nz,
 		unsigned i,
 		unsigned &x,
 		unsigned &y,
 		unsigned &z);

 	bool cellSurrounded(
 		unsigned i, 
 		const MIntArray &containment, 
 		unsigned nx, 
 		unsigned ny, 
 		unsigned nz) ;
	// bool cellSurrounded(
	// 	unsigned i, const MIntArray &containment, 
	// 	unsigned nx, unsigned ny, unsigned nz, MVector &normal) ;
MVector getNormal(
	unsigned i, const MIntArray &containment, 
	unsigned nx, unsigned ny, unsigned nz) ;

	qvoxData	*	geometry();
	
private:


	qvoxData * m_geomData;
	// GLuint m_box;

	
};

#endif
